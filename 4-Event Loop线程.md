# Event Loop

## 进程与线程

> ### 进程与线程区别？JS 单线程带来的好处

## 执行栈

> ### 什么是执行栈？

## 浏览器中的 Event Loop

> ### 异步代码执行顺序？解释一下什么是 Event Loop ？

## Node 中的 Event Loop

> ### Node 中的 Event Loop 和浏览器中的有什么区别？process.nexttick 执行顺序？

## 手写 call、apply 及 bind 函数

> ### call、apply 及 bind 函数内部实现是怎么样的？

## new

> ### new 的原理是什么？通过 new 的方式创建对象和通过字面量创建有什么区别？

## instanceof 的原理

> ### 涉及面试题：instanceof 的原理是什么？

## 为什么 0.1 + 0.2 != 0.3

> ### 为什么 0.1 + 0.2 != 0.3？如何解决这个问题？

## 垃圾回收机制

> ### V8 下的垃圾回收机制是怎么样的？

