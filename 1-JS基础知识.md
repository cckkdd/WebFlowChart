# 基础类型

### 原始类型有哪几种？null 是对象嘛？
### 对象类型和原始类型的不同之处？函数参数是对象会发生什么问题？
```mermaid
graph TD
	A(js类型)
	A-->B(原始数据)
		B-->B1(Number)
			B1-->0.1+0.2!=0.3
		B-->B2(String)
			B2-->'1'.toString
		B-->Null
		B-->Undefined
		B-->symbol
		B-->boolean
	A-->C(对象类型)
		C-->C1(Object)
			C1-->this

```
### Null和undefinded的区别是啥?
### NaN是干啥的

```mermaid
graph TD
a(类型判断)-->a1(typeof)-->|不能正取显示类型的|null
			a1--> 除了函数都不能显示正确类型
a-->instanceof
```

## 类型转换
### typeof 是否能正确判断类型？instanceof 能正确判断对象的原理是什么？

#### 正确判断 this？箭头函数的 this 是什么？
### == 和 === 有什么区别？
### 什么是闭包？
#### 循环中使用闭包解决 `var` 定义函数的问题
```
for (var i = 1; i <= 5; i++) {
  setTimeout(function timer() {
    console.log(i)
  }, i * 1000)
}
```
### 什么是浅拷贝？如何实现浅拷贝？什么是深拷贝？如何实现深拷贝？
### 如何理解原型？如何理解原型链？



# 常见数据结构

## 时间复杂度

## 栈

## 应用

## 队列

## 链表

## 树

## AVL 树

## Trie

##　并查集

 ## 堆

