# 异步
## 并发（concurrency）和并行（parallelism）区别
### 并发与并行的区别？
## 回调函数（Callback）
### 什么是回调函数？回调函数有什么缺点？如何解决回调地狱问题？
## Generator
### 你理解的 Generator 是什么？
## Promise
### Promise 的特点是什么，分别有什么优缺点？什么是 Promise 链？Promise 构造函数执行和 then 函数执行有什么区别？
## async 及 await
### async 及 await 的特点，它们的优点和缺点分别是什么？await 原理是什么？
## 常用定时器函数
### setTimeout、setInterval、requestAnimationFrame 各有什么特点？
## 手写 Promise
